import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SortWords {
    public HashMap<String, Integer> map(String str) {
       String[] words = str.split("[()—,;:.!?\\s]+");
       HashMap<String, Integer> word = new HashMap<>();
        for (String s : words) {
            if (s.length() == 0) {
                continue;
            }
            if (word.containsKey(s)) {
                word.put(s, word.get(s) + 1);
            } else word.put(s, 1);
        }
        return word;
    }
    public String[] repeat(HashMap<String, Long> word) {
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < map.size(); i++)
        {
            int a = map.get(i);
            int freg = Collections.frequency(map, a);
            System.out.println(a+" "+freg);
        }

    }
     public String[] unique(HashMap<String, Long> word) {
         Map<String, Integer> map = new HashMap<>();
         for (int i = 0; i<map.size(); i++)
         {
             int a = map.get(i);
             int freg = Collections.frequency(map,a);
             if (freg == 1)
                 System.out.println(word);
         }
    }
}

