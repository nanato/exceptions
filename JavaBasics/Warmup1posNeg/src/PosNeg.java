public class PosNeg {
    public static void main(String[] args) {
        boolean s = warmup1posNeg(-4, -5, true);
        System.out.println(s);
    }
    private static  boolean warmup1posNeg(int a, int b, boolean negative) {
        if (negative && a < 0 && b < 0) {
            return true;
        }
        else if (!negative && ((a < 0 && b > 0) || (a > 0 && b < 0))) {
            return true;
        }
        return false;
    }

}
